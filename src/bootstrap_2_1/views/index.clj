(ns bootstrap-2-1.views.index
  (:use [noir.core :only [defpage]])
  (:require [bootstrap-2-1.views.layout :as layout]
            [bootstrap-2-1.views.utils :as utils]))

(defpage "/welcome" []
  (layout/layout "Page Title"
    (utils/comment-html "This code is a ported version of the page found here: from http://twitter.github.com/bootstrap/examples/hero.html")
    [:div.navbar.navbar-inverse.navbar-fixed-top
      [:div.navbar-inner
        [:div.container
	        [:a.btn.btn-navbar {:data-toggle "collapse" :data-target ".nav-collapse"}
            [:span.icon-bar]
            [:span.icon-bar]
            [:span.icon-bar]]
          [:a.brand {:href "#"} "Project name"]
          [:div.nav-collapse.collapse
            [:ul.nav
              [:li.active [:a {:href "#"} "Home"]]
              [:li [:a {:href "#about"} "About"]]
              [:li [:a {:href "#contact"} "Contact"]]
              [:li.dropdown
                [:a.dropdown-toggle {:href "#" :data-toggle "dropdown"} "Dropdown"
                  [:b.caret]]
                [:ul.dropdown-menu
                  [:li [:a {:href "#"} "Action"]]
                  [:li [:a {:href "#"} "Another action"]]
                  [:li [:a {:href "#"} "Something else here"]]
                  [:li.divider]
                  [:li.nav-header "Nav header"]
                  [:li [:a {:href "#"} "Separated link"]]
                  [:li [:a {:href "#"} "One more separated link"]]]]]
            [:form.navbar-form.pull-right
              [:input.span2 {:type "text" :placeholder "Email"}]
              [:input.span2 {:type "password" :placeholder "Password"}]
              [:button.btn {:type "submit"} "Sign in"]]]]]]
    [:div.container
      (utils/comment-html "Main hero unit for a primary marketing message or call to action")
      [:div.hero-unit
        [:h1 "Hello, world!"]
        [:p "This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique."]
        [:p [:a.btn.btn-primary.btn-large "Learn more &raquo;"]]]
      (utils/comment-html "Example row of columns")
      [:div.row
        [:div.span4
          [:h2 "Heading"]
          [:p "Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui."]
          [:p [:a.btn {:href "#"} "View details &raquo;"]]]
        [:div.span4
          [:h2 "Heading"]
          [:p "Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui."]
          [:p [:a.btn {:href "#"} "View details &raquo;"]]]
        [:div.span4
          [:h2 "Heading"]
          [:p "Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus."]
          [:p [:a.btn {:href "#"} "View details &raquo;"]]]]
      [:hr]
      [:footer [:p "&copy; Company 2012"]]]))
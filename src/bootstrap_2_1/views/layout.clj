(ns bootstrap-2-1.views.layout
  (:use [noir.core :only [defpartial]]
        [hiccup.page :only [include-css include-js]])
  (:require [bootstrap-2-1.views.utils :as utils]
            [bootstrap-2-1.config :as config]))

(defpartial modernizr [& content]
  "<!doctype html>"
  (utils/comment-ie-hidden "lt IE 7" "<html class=\"no-js lt-ie9 lt-ie8 lt-ie7\" lang=\"en\">")
  (utils/comment-ie-hidden "IE 7" "<html class=\"no-js lt-ie9 lt-ie8\" lang=\"en\">")
  (utils/comment-ie-hidden "IE 8" "<html class=\"no-js lt-ie9\" lang=\"en\">")
  (utils/comment-ie-visible "gt IE 8" "<html class=\"no-js\" lang=\"en\">")
  content
  "</html>")

(defpartial page-head [title]
  [:head
    [:meta {:charset "utf-8"}]
    [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge,chrome=1"}]
    [:meta {:name "description" :content config/DESCRIPTION}]
    [:meta {:name "author" :content config/AUTHOR}]
    [:meta {:name "viewport" :content "width=device-width"}]
    [:title title]
    [:link {:href "http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Sans+Mono|Droid+Serif:400,700,400italic,700italic" :rel "stylesheet" :type "text/javascript"}]
    (utils/include-less "/less/style.less")
    (include-js "/js/libs/less-1.3.0.min.js"
                "/js/libs/modernizr-2.6.1-respond-1.1.0.min.js")])

(defpartial page-bottom []
  (include-js "//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js")
  (utils/inline-js "window.jQuery || document.write('<script src=\"/js/libs/jquery-1.8.0.min.js\"><\\/script>')")
  (include-js "/js/libs/bootstrap.js"
              "/js/script.js"))

(defpartial layout [title & content]
  (modernizr
    (page-head title)
    [:body
      content
      (page-bottom)]))
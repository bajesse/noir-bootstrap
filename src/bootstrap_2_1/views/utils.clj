(ns bootstrap-2-1.views.utils
  (:use [noir.core :only [defpartial]]))

(defpartial include-less [& styles]
  (for [style styles]
    [:link {:rel "stylesheet/less" :href style}]))

(defpartial inline-js [content]
  [:script {:type "text/javascript"} content])

(defpartial inline-css [content]
  [:style {:type "text/css"} content])

(defpartial comment-html [& content]
  "<!-- "
  content
  " -->")

(defpartial comment-ie-hidden [condition & content]
  "<!--[if " condition "]>"
  content
  "<![endif]-->")

(defpartial comment-ie-visible [condition & content]
    "<!--[if " condition "]><!-->"
    content
    "<!--<![endif]-->")